#include <asio/io_context.hpp>
#include <asio/post.hpp>
#include <asio/signal_set.hpp>
#include <asio/steady_timer.hpp>
#include <functional>
#include <iostream>
#include <memory>

using Callback = std::function<void()>;

class Motor
{
public:
  virtual ~Motor() = default;

  virtual void initialize(const Callback&) = 0;
  virtual void start(const Callback&) = 0;
};

class Service
{
public:
  static std::shared_ptr<Service> create(Motor& motor,
      asio::io_context& context)
  {
    auto shared = std::make_shared<Service>(motor, context);
    shared->self = shared;
    return shared;
  }

  Service(Motor& motor, asio::io_context& context) :
      motor{motor}, context{context}
  {
  }

  void start(const Callback& cb)
  {
    std::cout << "Service initialize request" << std::endl;

    callback = cb;
    motor.initialize([target = self]() {
      std::cout << "Service initialize start response" << std::endl;
      auto shared = target.lock();
      if (shared) {
        shared.get()->initializeDone();
      }
    });
  }

private:
  Motor& motor;
  asio::io_context& context;
  Callback callback{[]() {
  }};
  std::weak_ptr<Service> self{};

  void initializeDone()
  {
    std::cout << "Service start request" << std::endl;

    motor.start([target = self]() {
      std::cout << "Service start response" << std::endl;
      auto shared = target.lock();
      if (shared) {
        shared.get()->startDone();
      }
    });
  }

  void startDone()
  {
    std::cout << "Service done" << std::endl;
    context.post(callback);
  }
};

class FakeMotor: public Motor
{
public:
  FakeMotor(asio::io_context& context) :
      context{context}, timer{context, std::chrono::seconds(1)}
  {
  }

  void initialize(const Callback& callback) override
  {
    std::cout << "FakeMotor initialize" << std::endl;
    timer.async_wait([callback](const asio::error_code&) { callback(); });
  }

  void start(const Callback& callback) override
  {
    std::cout << "FakeMotor start" << std::endl;
    context.post(callback);
  }

private:
  asio::io_context& context;
  asio::steady_timer timer;
};

int main()
{
  asio::io_context context{};

  asio::signal_set signals{context, SIGQUIT, SIGTERM};
  signals.async_wait([&](const asio::error_code&, int) { context.stop(); });

  FakeMotor motor{context};
  auto service = Service::create(motor, context);

  std::cout << "Operator start request" << std::endl;
  service->start([&context] {
    std::cout << "Operator start response" << std::endl;
    context.stop();
  });

  // service.reset(); // Destroy service while waiting for response

  context.run();

  return 0;
}
